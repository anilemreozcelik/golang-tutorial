package main

import (
	"bufio"
	"fmt"
	"myapp/doctor"
	"os"
	"strings"
)

func main()  {

	//var whatToSay string="Hello,world!"
	//whatToSay:="Hello,world!"
	//whatToSay:=getWhatToSay()

	reader:=bufio.NewReader(os.Stdin)

	whatToSay:=doctor.Intro()
	printMessage(whatToSay)

	for {
		fmt.Print("-> ")
		userInput,_:=reader.ReadString('\n')
		userInput = strings.Replace(userInput,"\r\n","",-1)  //for windows

		if userInput=="quit" {
			break
		} else {
			response:=doctor.Response(userInput)
			fmt.Println(response)
		}
	}
}

func getWhatToSay() string{

	return "Hello,world!"
}

func printMessage(message string){
	fmt.Println(message)
}