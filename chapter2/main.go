package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"time"
)

const prompt = "and press ENTER when ready."

//Guess The Number Game
func main()  {

	//seed the number generator
	rand.Seed(time.Now().UnixNano())

	var firstNumber int=rand.Intn(10)+2
	var secondNumber =rand.Intn(10)+2
	subtraction:=rand.Intn(10)+2
	var answer= firstNumber * secondNumber - subtraction

	playTheGame(firstNumber,secondNumber,subtraction,answer)

}

func playTheGame(firstNumber,secondNumber,subtraction,answer int){
	fmt.Println(firstNumber)
	fmt.Println(secondNumber)
	fmt.Println(subtraction)
	reader:=bufio.NewReader(os.Stdin)


	fmt.Println("Guess the Number Game")
	fmt.Println("---------------------")
	fmt.Println("")

	fmt.Println("Think of a number between 1 and 10 ",prompt)
	reader.ReadString('\n')

	fmt.Println("Multiple your number by",firstNumber,prompt)
	reader.ReadString('\n')

	fmt.Println("Multiply the result by",secondNumber,prompt)
	reader.ReadString('\n')

	fmt.Println("Divide the result by the number you originally thought of",prompt)
	reader.ReadString('\n')

	fmt.Println("Now subtract",subtraction,prompt)
	reader.ReadString('\n')

	//give them the answer

	fmt.Println("The answer is",answer)
}